import numpy as np
import tensorflow as tf

from nonparametricflow.model.implementations.gaussian_processes.some_gp import some_gp
from nonparametricflow.model.implementations.kernels.some_kernel import some_kernel
from nonparametricflow.model.implementations.kernel_operators.some_operator import some_operator
import nonparametricflow.core.expressions.variable as variable
from nonparametricflow.core.expressions.misc import constant

np.random.seed(0)
tf.set_random_seed(0)

Y = None
X = None

t_X = constant(Y, name="Y")
t_Y = variable.any(X, name="X")

gp = some_gp(
    Y=t_Y,
    X=t_X,
    kernel=some_operator(kernels=[
        some_kernel(
            t_some_parameter=variable.non_negative(0, name="some parameter")
        ),
        some_kernel(
            t_some_parameter=variable.non_negative(0, name="some parameter")
        )
    ])
)


with tf.Session as session:

    t_cost = gp.cost.node

    # optimise

    # predict
    x = None
    t_pred_y_mean = gp.mean_predictor.node

    pred_y_mean = session.run(
        gp.mean_predictor.node,
        {
            gp.mean_predictor.placeholders["x"]: x
        }
    )

    # Get model specific parameter
    t_z = gp.exposed_values()["z"]

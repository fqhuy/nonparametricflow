
import tensorflow as tf


def optimise(
    session,
    t_optimiser,
    optimiser_params,
    callback_function,
    callback_at_iterations,
    num_iterations=1000
):
    callback_iteration = set(callback_at_iterations)

    optimiser = t_optimiser.minimize(optimiser_params.t_cost)

    # Important to initialize uninitialized variables after optimiser has been created
    session.run(tf.global_variables_initializer())

    session.graph.finalize()

    for iteration_index in range(num_iterations):

        opt, current_cost = session.run((optimiser, optimiser_params.t_cost), feed_dict=optimiser_params.feed_dict)

        if iteration_index not in callback_iteration:
            continue

        callback_function(
            iteration=iteration_index,
            current_cost=current_cost
        )


import tornado.web
import tornado.httpserver
import tornado.ioloop
import pickle


def start_server(session, gaussian_process, server_port):

    class PredictMeanHandler(tornado.web.RequestHandler):
        def post(self, *args):
            parameters = pickle.loads(self.request.body)
            mean_y = gaussian_process.predict_mean(x=parameters["x"], session=session)
            self.write(pickle.dumps(mean_y))
            self.set_status(200)

    class PredictVarianceHandler(tornado.web.RequestHandler):
        def post(self, *args):
            parameters = pickle.loads(self.request.body)
            variance_y = gaussian_process.predict_variance(x=parameters["x"], session=session)
            self.write(pickle.dumps(variance_y))
            self.set_status(200)

    class ParameterDictHandler(tornado.web.RequestHandler):
        def post(self, *args):
            parameter_dict = gaussian_process.parameter_dict(session=session)
            self.write(pickle.dumps(parameter_dict))
            self.set_status(200)

    def make_app():
        return tornado.web.Application([
            (r"/predict_mean", PredictMeanHandler),
            (r"/predict_variance", PredictVarianceHandler),
            (r"/parameter_dict", ParameterDictHandler)
        ])

    app = make_app()
    app.listen(server_port)

    tornado.ioloop.IOLoop.current().start()

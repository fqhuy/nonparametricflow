
import tensorflow as tf
import numpy as np
from nonparametricflow.interface.data_type import dtype


def constant(value, name=None):
    return tf.constant(value, dtype=dtype, name=name)


def entropy(t_probabilities):
    return -tf.reduce_sum(t_probabilities * tf.log(t_probabilities))
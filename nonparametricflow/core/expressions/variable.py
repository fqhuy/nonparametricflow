
import tensorflow as tf
import numpy as np

from nonparametricflow.interface.data_type import dtype


EPSILON = 0.000001


def non_negative(initial_value, name=None):
    return tf.sqrt(tf.square(tf.Variable(initial_value, dtype=dtype)), name=name)


def any(initial_value, name=None):
    return tf.Variable(initial_value, dtype=dtype, name=name)
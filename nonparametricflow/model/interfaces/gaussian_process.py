
import numpy as np
import tensorflow as tf


class Expression(object):

    def __init__(self, node, placeholders):
        self.node = node
        self.placeholders = placeholders


class GaussianProcess(object):

    def __init__(
            self,
            cost,
            mean_predictor,
            variance_predictor,
            exposed_values,
    ):
        self.cost = cost
        self.mean_predictor = mean_predictor
        self.variance_predictor = variance_predictor
        self.exposed_values = exposed_values

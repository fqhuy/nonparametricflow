
import tensorflow as tf


class Kernel(object):

    def __init__(
        self,
        t_prior,
        t_covariance_matrix_function,
        t_covariance_diag_function,
        parameter_dict_function
    ):
        self.t_prior = t_prior
        self._t_covariance_matrix_function = t_covariance_matrix_function
        self._t_covariance_diag_function = t_covariance_diag_function
        self._parameter_dict_function = parameter_dict_function

    def t_covariance_matrix(self, t_X_0, t_X_1):
        return self._t_covariance_matrix_function(t_X_0=t_X_0, t_X_1=t_X_1)

    def t_covariance_diag(self, t_X):
        return self._t_covariance_diag_function(t_X=t_X)

    def parameter_dict(self, session: tf.Session) -> dict:
        return self._parameter_dict_function(session)

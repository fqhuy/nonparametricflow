
from nonparametricflow.model.interfaces.kernel import Kernel


def some_kernel(
    t_some_parameter,
):
    def t_covariance_matrix(t_X_0, t_X_1):
        return None

    def t_covariance_diag(t_X):
        return None

    def parameter_dict(session):
        return {
            "name": "Some kernel",
        }

    return Kernel(
        t_prior=None,
        t_covariance_matrix_function=t_covariance_matrix,
        t_covariance_diag_function=t_covariance_diag,
        parameter_dict_function=parameter_dict,
    )
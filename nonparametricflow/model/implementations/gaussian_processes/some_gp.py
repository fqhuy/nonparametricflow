
import scipy.sparse.linalg
import tensorflow as tf
import numpy as np

from nonparametricflow.model.interfaces.gaussian_process import GaussianProcess, Expression
from nonparametricflow.model.interfaces.kernel import Kernel


def some_gp(
    Y,
    X,
    kernel: Kernel,
) -> GaussianProcess:
    return GaussianProcess(
        cost=Expression(
            node=None,
            placeholders={
                "y": None
            }
        ),
        mean_predictor=Expression(
            node=None,
            placeholders={
                "x": None
            }
        ),
        variance_predictor=Expression(
            node=None,
            placeholders={
                "x": None
            }
        ),
        exposed_values={
            "some_field": None,
            "some_other_field": None
        }
    )


import tensorflow as tf
from nonparametricflow.model.interfaces.kernel import Kernel


def some_operator(
    kernels,
):
    def t_covariance_matrix(t_X_0, t_X_1):
        return tf.add_n(
            [
                kernel.t_covariance_matrix(t_X_0=t_X_0, t_X_1=t_X_1)
                for kernel in kernels
            ]
        )

    def t_covariance_diag(t_X):
        return tf.add_n(
            [
                kernel.t_covariance_diag(t_X=t_X)
                for kernel in kernels
            ]
        )

    def parameter_dict(session):
        return {
            "name": "Some kernel operator",
        }

    return Kernel(
        t_prior=None,
        t_covariance_matrix_function=t_covariance_matrix,
        t_covariance_diag_function=t_covariance_diag,
        parameter_dict_function=parameter_dict,
    )
